--[[
__autor__   = "Alexander Olivares (aox)"
__email__   = "olivaresa@gmail.com"
__date__    = "2012-10-25"
__version__ = "beta-0.1"
__license__ = "GPLv2"
]]

local feeds
local feeds_count = 0
local rssIndex = 0
local itemIndex = 0
local category_color = "red"
local dx, dy = canvas:attrSize()


function screen_px(percentage)
    return percentage * dx / 100
end
function screen_py(percentage)
    return percentage * dy / 100
end

local imageX = screen_px(74.125)-125 
local imageY = screen_py(67.06)-79
local textX = screen_px(7.5)
local textY = screen_py(22.5)

local keys = {RED="red", GREEN="green", YELLOW="yellow", BLUE="blue",
            F1="red", F2="green", F3="yellow", F4="blue"}



function breakString(str, maxLineSize)
  local t = {}
  local i, fim, countLns = 1, 0, 0

  if (str == nil) or (str == "") then
     return t
  end 

  --str = string.gsub(str, "\n", " ")
  --str = string.gsub(str, "\r", " ")
    
  while i < #str do
     countLns = countLns + 1
     if i > #str then
        t[countLns] = str
        i = #str 
     else
        fim = i+maxLineSize-1
        if fim > #str then
           fim = #str
        else

            if string.byte(str, fim) ~= 32 then
               fim = string.find(str, ' ', fim)
               if fim == nil then
                  fim = #str
               end
            end
        end
        
        str_line = string.sub(str, i, fim)
        index_str_n = string.find(str_line, '\n')
        
        if index_str_n then
            fim = i + index_str_n - 1
        end
        
        str_line = string.sub(str, i, fim)
        str_line = string.gsub(str_line, "\n", "")
        str_line = string.gsub(str_line, "\r", "")
        t[countLns]=str_line
        i=fim+1
     end
  end
  
  return t
end


function limpiar()
    canvas:attrColor(0,0,0,0)
    canvas:drawRect("fill",0, screen_py(30.5),screen_px(100),screen_py(100)) --limpia noticia e imagen
    
end

function read_Feeds()
       
    local json = require("json")
    local data = require("data")
        
    feeds = json.decode(data.FEEDS)

end

function get_item_index(key)
    
    if feeds.category[category_color] then
        feeds_count = #feeds.category[category_color].rss
    else
        limpiar()
        canvas:attrFont("Tiresias", 14, "bold")
        canvas:drawText(textX, textY + screen_py(7.5), "No existe información para esta categoría...")
        
        image = canvas:new("../media/0.jpg")
        canvas:compose(imageX, imageY, image)
        
        canvas:flush()
        return
    end
    
    if feeds_count > 0 then
        if rssIndex == 0 then 
            rssIndex = 1
        end
    else
        return
    end
    
    if key == "r" then
        if (itemIndex + 1) <= #feeds.category[category_color].rss[rssIndex].channel.item then
            itemIndex = itemIndex + 1
        else
            if (rssIndex + 1) <= feeds_count then
                rssIndex = rssIndex + 1
                itemIndex = 1
            end
        end
    end

    if key == "l" then
        if (itemIndex - 1) <= #feeds.category[category_color].rss[rssIndex].channel.item and (itemIndex - 1) > 0 then
            itemIndex = itemIndex - 1
        else
            if (rssIndex - 1) <= feeds_count and (rssIndex - 1) > 0 then
                rssIndex = rssIndex - 1
                itemIndex = #feeds.category[category_color].rss[rssIndex].channel.item
            end
        end
    end

    limpiar()
    
    canvas:attrFont("Tiresias", 16, "bold")

    local linhas = breakString(feeds.category[category_color].rss[rssIndex].channel.item[itemIndex].title, 28)

    texty = textY + 45 

    for k,ln in pairs(linhas) do
        texty = texty + 12 + 3
        canvas:drawText(textX, texty, ln)
    end

    texty = texty + 20
    
    canvas:attrFont("Tiresias", 10)
    canvas:drawText(textX, texty + screen_py(3.33), feeds.category[category_color].rss[rssIndex].channel.item[itemIndex].pubDate)

    
    local linhas = breakString(string.sub(feeds.category[category_color].rss[rssIndex].channel.item[itemIndex].description,1,380) .. " ...", 38)
    
    texty = texty + 20 
    canvas:attrFont("Tiresias", 13)
    for k,ln in pairs(linhas) do
        texty = texty + 12 + 3
        canvas:drawText(textX, texty, ln)
    end


    canvas:attrFont("Tiresias", 10, "bold")
    canvas:drawText(textX, texty + screen_py(5.83), feeds.category[category_color].rss[rssIndex].channel.item[itemIndex].link)
    
    image = canvas:new("../media/" .. feeds.category[category_color].rss[rssIndex].channel.item[itemIndex].image) 
    
    image:attrColor(255,0,0,0)
    image:drawRect("frame",0,0,250,158)
    image:drawRect("frame",1,1,248,156)

    canvas:compose(imageX, imageY, image)
    canvas:flush()
    
end

function set_feed_background_color(color)

    local tx = screen_px(6.25)
    local ty = screen_py(27)
    
    canvas:attrColor(0,0,0,0)
    canvas:drawRect("fill", 0, screen_py(24.5),screen_px(100) ,screen_py(6)) -- limpia titulo
    canvas:attrFont("Tiresias", 20, "bold")

    if color == "red" then 
        canvas:attrColor(164,0,27,0)
        canvas:drawText(tx, ty, feeds.config.category.red)
        
    end
    if color == "green" then 
        canvas:attrColor(0,144,56,0)
        canvas:drawText(tx, ty, feeds.config.category.green)
    end
    if color == "yellow" then 
        canvas:attrColor(228,172,5,0)
        canvas:drawText(tx, ty, feeds.config.category.yellow)
    end
    if color == "blue" then 
        canvas:attrColor(0,77,174,0)
        canvas:drawText(tx, ty, feeds.config.category.blue)
    end
    
    canvas:flush()
end

function handler(evt)

    if evt.class == 'key' and  evt.type == 'press' then 

        local key = evt.key

        if key == "CURSOR_LEFT" then
            get_item_index("l")
        end 

        if key == "CURSOR_RIGHT" then
            get_item_index("r")
        end 
            
        if keys[key] then
        
            lower_key = keys[key]
            
            set_feed_background_color(lower_key)
            category_color = lower_key
            feeds_count = 0
            rssIndex = 1
            itemIndex = 1
            get_item_index(lower_key)
        end 
    end
end

read_Feeds()
set_feed_background_color("red")
get_item_index("r")
event.register(handler)





