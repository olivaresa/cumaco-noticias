<?php

function cambiartam($archivo,$ancho,$alto,$url) 
{

   $data = file_get_contents($url);

   if(!$data)
     return false;

   $imagen=imagecreatefromstring($data);
   $x=imageSX($imagen); 
   $y=imageSY($imagen); 
   $nueva_img = ImageCreateTrueColor($ancho,$alto);      
   imagecopyresampled($nueva_img,$imagen,0,0,0,0,$ancho,$alto,$x,$y);  
   imagejpeg($nueva_img,$archivo);  
   imagedestroy($nueva_img);  
   imagedestroy($imagen);  
   
   return true;
}
?>
