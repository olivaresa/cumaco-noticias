<?php 

echo "Generando fichero data.lua";

include_once ("conexion.php");
$link=Conectarse();

$str_datos = file_get_contents("archivo.json");

$datos = json_decode($str_datos,true);

function elimina_acentos($cadena){
$tofind = "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ";
$replac = "AAAAAAaáaaaaOOOOOOoóooooEEEEeéeeCcIIIIiíiiUUUUuúuuyNñ";
return (strtr($cadena,utf8_decode($tofind),$replac));

}

$sql1 = "SELECT * FROM contenido where src_id='1'";

$resultado1 = mysql_query($sql1,$link);

$numero = mysql_num_rows($resultado1);
           
if ($numero != 0){      
    for ($i=0; $i<$numero; $i++) {



        $ssql4 = "SELECT * FROM src where id='1'";
        $result4 = mysql_query($ssql4,$link);
        $fila = mysql_fetch_array($result4);
        $aa=$fila['titulo'];
        $bb=$fila['enlace'];    

        $row=mysql_fetch_array($resultado1);
        $a=$row['title'];
        $b=$row['link'];
        $c=$row['description'];
        $d=$row['pubDate'];
        $e=$row['imagen'];


        $datos[0][0]["category"]["red"]["rss"][0]["channel"]["title"]= $aa;
        $datos[0][0]["category"]["red"]["rss"][0]["channel"]["link"]= $bb;
        $datos[0][0]["category"]["red"]["rss"][0]["channel"]["item"][$i]["title"]= $a;
        $datos[0][0]["category"]["red"]["rss"][0]["channel"]["item"][$i]["link"]= $b;
        $datos[0][0]["category"]["red"]["rss"][0]["channel"]["item"][$i]["description"]= strip_tags($c);
        $datos[0][0]["category"]["red"]["rss"][0]["channel"]["item"][$i]["pubDate"]= $d;
        $datos[0][0]["category"]["red"]["rss"][0]["channel"]["item"][$i]["image"]= $e;

    }

}

$sql2 = "SELECT * FROM contenido where src_id='2'";

$resultado2 = mysql_query($sql2,$link);

$numero = mysql_num_rows($resultado2);
           
if ($numero != 0){
    
    for ($i=0; $i<$numero; $i++) {

        $ssql4 = "SELECT * FROM src where id='2'";
        $result4 = mysql_query($ssql4,$link);
        $fila = mysql_fetch_array($result4);
        $aa=$fila['titulo'];
        $bb=$fila['enlace'];        
    
        $row=mysql_fetch_array($resultado2);
        $a=$row['title'];
        $b=$row['link'];
        $c=$row['description'];
        $d=$row['pubDate'];
        $e=$row['imagen'];


        $datos[0][0]["category"]["green"]["rss"][0]["channel"]["title"]= $aa;
        $datos[0][0]["category"]["green"]["rss"][0]["channel"]["link"]= $bb;
        $datos[0][0]["category"]["green"]["rss"][0]["channel"]["item"][$i]["title"]= $a;
        $datos[0][0]["category"]["green"]["rss"][0]["channel"]["item"][$i]["link"]= $b;
        $datos[0][0]["category"]["green"]["rss"][0]["channel"]["item"][$i]["description"]= strip_tags($c);
        $datos[0][0]["category"]["green"]["rss"][0]["channel"]["item"][$i]["pubDate"]= $d;
        $datos[0][0]["category"]["green"]["rss"][0]["channel"]["item"][$i]["image"]= $e;
    }

}

$sql3 = "SELECT * FROM contenido where src_id='3'";

$resultado3 = mysql_query($sql3,$link);

$numero = mysql_num_rows($resultado3);
           
if ($numero != 0){      
    for ($i=0; $i<$numero; $i++) {
        
        $ssql4 = "SELECT * FROM src where id='3'";
        $result4 = mysql_query($ssql4,$link);
        $fila = mysql_fetch_array($result4);
        $aa=$fila['titulo'];
        $bb=$fila['enlace'];        


        $row=mysql_fetch_array($resultado3);
        $a=$row['title'];
        $b=$row['link'];
        $c=$row['description'];
        $d=$row['pubDate'];
        $e=$row['imagen'];

    
        $datos[0][0]["category"]["yellow"]["rss"][0]["channel"]["title"]= $aa;
        $datos[0][0]["category"]["yellow"]["rss"][0]["channel"]["link"]= $bb;
        $datos[0][0]["category"]["yellow"]["rss"][0]["channel"]["item"][$i]["title"]= $a;
        $datos[0][0]["category"]["yellow"]["rss"][0]["channel"]["item"][$i]["link"]= $b;
        $datos[0][0]["category"]["yellow"]["rss"][0]["channel"]["item"][$i]["description"]= strip_tags($c);
        $datos[0][0]["category"]["yellow"]["rss"][0]["channel"]["item"][$i]["pubDate"]= $d;
        $datos[0][0]["category"]["yellow"]["rss"][0]["channel"]["item"][$i]["image"]= $e;
    }

}

$sql4 = "SELECT * FROM contenido where src_id='4'";

$resultado4 = mysql_query($sql4,$link);

$numero = mysql_num_rows($resultado4);
           
if ($numero != 0){  
    for ($i=0; $i<$numero; $i++) {

        $ssql4 = "SELECT * FROM src where id='4'";
        $result4 = mysql_query($ssql4,$link);
        $fila = mysql_fetch_array($result4);

        $aa=$fila['titulo'];
        $bb=$fila['enlace'];    
        $row=mysql_fetch_array($resultado4);
        $a=$row['title'];
        $b=$row['link'];
        $c=$row['description'];
        $d=$row['pubDate'];
        $e=$row['imagen'];
    
        $datos[0][0]["category"]["blue"]["rss"][0]["channel"]["title"]= $aa;
        $datos[0][0]["category"]["blue"]["rss"][0]["channel"]["link"]= $bb;
        $datos[0][0]["category"]["blue"]["rss"][0]["channel"]["item"][$i]["title"]= $a;
        $datos[0][0]["category"]["blue"]["rss"][0]["channel"]["item"][$i]["link"]= $b;             
        $datos[0][0]["category"]["blue"]["rss"][0]["channel"]["item"][$i]["description"]= strip_tags($c);
        $datos[0][0]["category"]["blue"]["rss"][0]["channel"]["item"][$i]["pubDate"]= $d;
        $datos[0][0]["category"]["blue"]["rss"][0]["channel"]["item"][$i]["image"]= $e;
    }

}

function jsonRemoveUnicodeSequences($struct) 
{
   return preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", json_encode($struct));

}

$proc= jsonRemoveUnicodeSequences($datos);

$DescriptorFichero = fopen("../CumacoNoticias/lua_scripts/data.lua","w");

$palabra= 'module("data")';

fputs($DescriptorFichero,$palabra);

$palabra2= "\n";

fputs($DescriptorFichero,$palabra2);

$palabras3= 'FEEDS=';

fputs($DescriptorFichero,$palabras3);

fputs($DescriptorFichero,$palabra2);;

fclose($DescriptorFichero);



$fh = fopen("../CumacoNoticias/lua_scripts/data.lua", 'a+')
      or die("Error al abrir fichero de salida");

fwrite($fh, $proc);

fclose($fh);

///////////////////////////////////////////////
// Generar carpeta de la aplicación ginga actualizada
///////////////////////////////////////////////

//$last_line_ginga = system('sh -c ./generar_app_ginga.sh', $retval_ginga);

?>
